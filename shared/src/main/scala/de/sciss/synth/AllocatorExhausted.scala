/*
 *  AllocatorExhausted.scala
 *  (ScalaCollider)
 *
 *  Copyright (c) 2008-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.synth

final case class AllocatorExhausted(reason: String) extends RuntimeException(reason)
