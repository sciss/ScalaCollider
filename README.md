# ScalaCollider

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.sciss/scalacollider_2.13/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.sciss/scalacollider_2.13)

## statement

ScalaCollider is a [SuperCollider](https://supercollider.github.io/) client for the Scala programming language. 
It is (C)opyright 2008&ndash;2025 by Hanns Holger Rutz. All rights reserved. ScalaCollider is released under
the [GNU Affero General Public License](https://codeberg.org/sciss/ScalaCollider/raw/branch/main/LICENSE) v3+ and comes
with absolutely no warranties. To contact the author, send an e-mail to `contact at sciss.de`.

SuperCollider is one of the most elaborate open source sound synthesis frameworks. It comes with its own language
'SCLang' that controls the sound synthesis processes on a server, 'scsynth'. ScalaCollider is an alternative to
'SCLang', giving you the (perhaps) familiar Scala language to express these sound synthesis processes, and letting
you hook up any other Scala, Java or JVM-based libraries. ScalaCollider's function is more reduced than 'SCLang',
focusing on UGen graphs and server-side resources such as buses and buffers. Other functionality is part of the 
standard Scala library, e.g. collections and GUI. Other functionality, such as plotting, MIDI, client-side 
sequencing (`Pdef`, `Routine`, etc.) must be added through dedicated libraries (see section 'packages' below).

While ScalaCollider itself is in the form of a _library_, you can use it inside a plain REPL, or via the
[ScalaCollider-Swing](https://codeberg.org/sciss/ScalaColliderSwing) project that adds an easy-to-use standalone 
application or mini-IDE. On the ScalaCollider-Swing page, you'll find a link to download a readily compiled binary 
for this standalone version.

__Note:__ An even more elaborate way to use ScalaCollider, is through [SoundProcesses](https://codeberg.org/sciss/SoundProcesses)
and its graphical front-end [Mellite](https://sciss.de/mellite).

## download and resources

The current version of ScalaCollider (the library) can be downloaded
from [codeberg.org/sciss/ScalaCollider](https://codeberg.org/sciss/ScalaCollider).

More information is available from the wiki
at [codeberg.org/sciss/ScalaCollider/wiki](https://codeberg.org/sciss/ScalaCollider/wiki). The API documentation is
available at [sciss.de/scalaCollider/latest/api](https://sciss.de/scalaCollider/latest/api/de/sciss/synth/index.html).

The best way to ask questions, no matter if newbie or expert, is to use the mailing list
at [groups.google.com/group/scalacollider](http://groups.google.com/group/scalacollider). To subscribe, simply
send a mail to `ScalaCollider+subscribe@googlegroups.com` (you will receive a mail asking for confirmation).

The early architectural design of ScalaCollider is documented in the SuperCollider 2010 symposium proceedings:
[H.H.Rutz, Rethinking the SuperCollider Client...](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.186.9817&rep=rep1&type=pdf).
However, many design decisions have been revised or refined in the meantime.

The file [ExampleCmd.sc](https://codeberg.org/sciss/ScalaCollider/src/branch/main/ExampleCmd.sc) is a good
starting point for understanding how UGen graphs are written in ScalaCollider. You can directly copy and paste these
examples into the ScalaCollider-Swing application's interpreter window.

See the section 'starting a SuperCollider server' below, for another simple example of running a server (possibly
from your own application code).

## building

ScalaCollider builds with [sbt](http://scala-sbt.org/) against Scala 2.13, 2.12, Dotty (JVM) and Scala 2.13 (JS).
The last version to support Scala 2.11 was 1.28.4.
Note that because of an incompatibility between ScalaOSC and Scala.js, support for Scala.js is currently incomplete,
and consequently some OSC related tests are not run under Scala.js.

ScalaCollider requires SuperCollider server to be installed and/or running. The recommended version as of
this writing is 3.10. Note that the UGens are provided by the
separate [ScalaColliderUGens](https://codeberg.org/sciss/ScalaColliderUGens) project. A simple Swing front end is
provided by the [ScalaColliderSwing](https://codeberg.org/sciss/ScalaColliderSwing) project.

Targets for sbt:

- `clean` &ndash; removes previous build artefacts
- `compile` &ndash; compiles classes into target/scala-version/classes
- `doc` &ndash; generates api in target/scala-version/api/index.html
- `package` &ndash; packages jar in target/scala-version
- `rootJVM/console` &ndash; opens a Scala REPL with ScalaCollider on the classpath

## linking

To use this project as a library, use the following artifact:

    libraryDependencies += "de.sciss" %% "scalacollider" % v

The current version `v` is `"2.7.5"`

## contributing

Please see the file [CONTRIBUTING.md](CONTRIBUTING.md)


## REPL test

Here is how a quick REPL check looks. `sbt rootJVM/console` will start with default Scala (2.13),
while `sbt ++3.1.0 rootJVM/console` allows you to use the new Scala 3.

```scala
$ sbt rootJVM/console
[info] welcome to sbt 1.5.2 (Debian Java 11.0.11)
...
[info] loading settings for project scalacollider-build from plugins.sbt ...
[info] loading project definition from ~/Documents/devel/ScalaCollider/project
[info] loading settings for project scalacollider from build.sbt ...
[info] set current project to scalacollider (in build file:~/Documents/devel/ScalaCollider/)
[info] Starting scala interpreter...
Welcome to Scala 2.13.5 (OpenJDK 64-Bit Server VM, Java 11.0.11).
Type in expressions for evaluation. Or try :help.
import de.sciss.osc
import de.sciss.synth._
import de.sciss.synth.ugen._
import Predef.{any2stringadd=>_, _}
import Import._
import Ops._
def s: de.sciss.synth.Server
def boot(): Unit

scala> boot()

scala> Found 109 LADSPA plugins
JackDriver: client name is 'SuperCollider'
SC_AudioDriver: sample rate = 48000.000000, driver's block size = 1024
SuperCollider 3 server ready.
JackDriver: max output latency 42.7 ms

scala> play {
| val f = LFSaw.ar(20).linExp(-1, 1, 300, 600)
| val s = SinOsc.ar(Seq(f, f * 1.01))
| s * 0.3
| }
val res1: de.sciss.synth.Synth = Synth(<localhost>,1000) : <temp_1>

scala> s.freeAll()
```

An alternative terminal REPL is [Ammonite](https://ammonite.io/#Ammonite-REPL). If you have it installed, you
can launch it with `amm` and "import" ScalaCollider: 

```scala
$ amm
Loading...
Welcome to the Ammonite Repl 2.4.0 (Scala 2.13.6 Java 11.0.11)
@ import $ivy.`de.sciss::scalacollider:2.6.4`, de.sciss.synth._, Import._, Ops._, ugen._ 
import $ivy.$                              , de.sciss.synth._, Import._, Ops._, ugen._

@ Server.run(_ => ()) 

@ Found 109 LADSPA plugins
JackDriver: client name is 'SuperCollider'
SC_AudioDriver: sample rate = 48000.000000, driver's block size = 1024
SuperCollider 3 server ready.
JackDriver: max output latency 42.7 ms
@ play { WhiteNoise.ar(Seq.fill(2)(0.3)) } 
res2: Synth = Synth(server = <localhost>, id = 1000)

@ Server.default.freeAll()
  
@ exit 
Bye!
```

## starting a SuperCollider server

Inside a regular Scala source code file,
the following short example illustrates how a server can be launched, and a synth played:

```scala
import de.sciss.synth._
import ugen._
import Import._
import Ops._

val cfg = Server.Config()
cfg.program = "/path/to/scsynth"
// runs a server and executes the function
// when the server is booted, with the
// server as its argument 
Server.run(cfg) { s =>
  s.dumpOSC()
  // `play` is imported from object `Ops`.
  // It provides a convenience method for wrapping
  // a synth graph function in an `Out` element
  // and playing it back.
  play {
    val f = LFSaw.kr(0.4).mulAdd(24, LFSaw.kr(Seq(8, 7.23)).mulAdd(3, 80)).midiCps
    CombN.ar(SinOsc.ar(f) * 0.04, 0.2, 0.2, 4)
  }
}    
```

For more sound examples, see `ExampleCmd.sc`. There is also an introductory video for
the [Swing frontend](https://codeberg.org/sciss/ScalaColliderSwing)
at [www.screencast.com/t/YjUwNDZjMT](http://www.screencast.com/t/YjUwNDZjMT), and some of
the [Mellite tutorials](https://www.sciss.de/mellite/tutorials.html) also introduce ScalaCollider concepts.

__Troubleshooting:__ If the above boots the server, but on Linux you do not 
hear any sound, probably the Jack audio server does not establish connections between
SuperCollider and your sound card. The easiest is to use a program such as QJackCtl
to automatically wire them up. Alternatively, you can set environment variables
`SC_JACK_DEFAULT_INPUTS` and `SC_JACK_DEFAULT_OUTPUTS` before starting Scala, e.g.

```bash
export SC_JACK_DEFAULT_INPUTS="system:capture_1,system:capture_2"
export SC_JACK_DEFAULT_OUTPUTS="system:playback_1,system:playback_2"
```

### Specifying `SC_HOME`

__Note__: This section is mostly irrelevant on Linux, where `scsynth` is normally found on `$PATH`, and thus no
further customisation is needed.

You might omit to set the `program` of the server's configuration, as ScalaCollider will by default read the
system property `SC_HOME`, and if that is not set, the environment variable `SC_HOME`. Environment variables are
stored depending on your operating system.
For example, if you run ScalaCollider from a Bash terminal, you edit `~/.bash_profile`. The entry
is something like:

    export SC_HOME=/path/to/folder-of-scsynth

On linux, the environment variables probably go in `~/.profile` or `~/.bashrc`.

## packages

ScalaCollider's core functionality may be extended by other libraries I or other people wrote. The following three
libraries are dependencies and therefore always available in ScalaCollider:

- UGens are defined by the [ScalaCollider-UGens](https://codeberg.org/sciss/ScalaColliderUGens) library.
- Audio file functionality is provided by the [AudioFile](https://codeberg.org/sciss/AudioFile) library.
- Open Sound Control functionality is provided by the [ScalaOSC](https://codeberg.org/sciss/ScalaOSC) library.

Here are some examples for libraries not included:

- Patterns functionality is  available through the [Patterns](https://codeberg.org/sciss/Patterns) library, which
  is supported through
  [SoundProcesses](https://codeberg.org/sciss/SoundProcesses) rather than vanilla ScalaCollider. SoundProcesses also
  takes the role of more high-level abstractions similar to `NodeProxy`, for example.
- MIDI functionality can be added with the [ScalaMIDI](https://codeberg.org/sciss/ScalaMIDI) library.
- Plotting is most easily achieved through [Scala-Chart](https://codeberg.org/sciss/scala-chart), which is 
  conveniently included in ScalaCollider-Swing.
