addSbtPlugin("com.eed3si9n" % "sbt-buildinfo"   % "0.13.1")  // provides version information to copy into main class
addSbtPlugin("com.typesafe" % "sbt-mima-plugin" % "1.1.4")   // binary compatibility testing
addSbtPlugin("org.scala-js" % "sbt-scalajs"     % "1.13.2")   // cross-compile for scala.js
addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject" % "1.3.2")

