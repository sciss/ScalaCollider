lazy val baseName        = "ScalaCollider"
lazy val baseNameL       = baseName.toLowerCase

lazy val BASE_VERSION    = "2.7.4"
lazy val PROJECT_VERSION = BASE_VERSION

lazy val deps = new {
  val audioFile          = "2.4.2"
  val scalaColliderSwing = "2.9.2"
  val scalaColliderUGens = "1.21.4"
  val scalaOSC           = "1.3.1"
}

val scalaColliderURI    = uri(s"https://codeberg.org/sciss/$baseName.git#v${BASE_VERSION}")
val lScalaCollider      = ProjectRef(scalaColliderURI, "rootJVM")
val xScalaCollider      = ProjectRef(scalaColliderURI, "rootJS")

val audioFileURI        = uri(s"https://codeberg.org/sciss/AudioFile.git#v${deps.audioFile}")
val lAudioFile          = ProjectRef(audioFileURI, "rootJVM")
val xAudioFile          = ProjectRef(audioFileURI, "rootJS")

val scalaColliderSwingURI  = uri(s"https://codeberg.org/sciss/ScalaColliderSwing.git#v${deps.scalaColliderSwing}")
val lScalaColliderSwing  = RootProject(scalaColliderSwingURI)

val scalaColliderUGensURI      = uri(s"https://codeberg.org/sciss/ScalaColliderUGens.git#v${deps.scalaColliderUGens}")
//val scalaColliderUGensURI = uri("https://codeberg.org/sciss/ScalaColliderUGens.git#f59f3663b292fc9846ef274b6653dd3aa3ca218d") // unidoc problem fix
val lScalaColliderUGensAPI     = ProjectRef(scalaColliderUGensURI, "apiJVM")
val xScalaColliderUGensAPI     = ProjectRef(scalaColliderUGensURI, "apiJS")
val lScalaColliderUGensCore    = ProjectRef(scalaColliderUGensURI, "coreJVM")
val xScalaColliderUGensCore    = ProjectRef(scalaColliderUGensURI, "coreJS")
val lScalaColliderUGensPlugins = ProjectRef(scalaColliderUGensURI, "pluginsJVM")
val xScalaColliderUGensPlugins = ProjectRef(scalaColliderUGensURI, "pluginsJS")

val scalaOSCURI         = uri(s"https://codeberg.org/sciss/ScalaOSC.git#v${deps.scalaOSC}")
val lScalaOSC           = ProjectRef(scalaOSCURI, "rootJVM")
val xScalaOSC           = ProjectRef(scalaOSCURI, "rootJS")

lazy val lList = Seq(
  lAudioFile, 
  lScalaCollider, 
  lScalaColliderUGensAPI, lScalaColliderUGensCore, lScalaColliderUGensPlugins,
  lScalaColliderSwing, 
  lScalaOSC,
)

ThisBuild / scalaVersion := "2.13.6"

lazy val commonSettings = Seq(
  scalaVersion := "2.13.6"
)

lazy val unidocSettings = Seq(
  Compile / packageDoc / mappings := (mappings in (ScalaUnidoc, packageDoc)).value,
  unidocProjectFilter in (ScalaUnidoc, unidoc) := inAnyProject -- inProjects(
    xAudioFile, 
    xScalaColliderUGensAPI, xScalaColliderUGensCore, xScalaColliderUGensPlugins,
    xScalaCollider,
    xScalaOSC,
  ),
  libraryDependencies ++= Seq(
    "org.scala-lang" % "scala-reflect" % scalaVersion.value // % "provided" // this is needed for sbt-unidoc to work with macros used by Mellite!
  ),
  Compile / doc / scalacOptions ++= Seq(
    "-skip-packages", Seq(
      "de.sciss.osc.impl", 
      "de.sciss.synth.impl",
      "snippets"
    ).mkString(":"),
    "-doc-title", s"${baseName} ${PROJECT_VERSION} API"
  )
) 

////////////////////////// site

val site = project.withId(s"$baseNameL-site").in(file("."))
  .enablePlugins(ParadoxSitePlugin, /* GhpagesPlugin, */ ScalaUnidocPlugin, SiteScaladocPlugin)
  .settings(commonSettings)
  .settings(unidocSettings)
  .settings(
    name                 := baseName, // IMPORTANT: `name` is used by GhpagesPlugin, must base base, not s"$baseName-Site"!
    version              := PROJECT_VERSION,
    SiteScaladoc / siteSubdirName := "latest/api",
    paradoxTheme         := Some(builtinParadoxTheme("generic")),
    paradoxProperties /* in Paradox */ ++= Map(
      "snippet.base_dir"        -> s"${baseDirectory.value}/snippets/src/main",
      "image.base_url"          -> "assets/images",
      "swingversion"            -> deps.scalaColliderSwing,
      "extref.swingdl.base_url" -> s"https://codeberg.org/sciss/ScalaColliderSwing/releases/download/v${deps.scalaColliderSwing}/ScalaColliderSwing_${deps.scalaColliderSwing}%s"
    ),
    paradoxRoots := List("index.html"), // you need that if there is no TOC
  )
  .aggregate(lList: _*)

/* currently doesn't work BECAUSE SBT 
val snippets = project.withId(s"$baseNameL-snippets").in(file("snippets"))
  .dependsOn(lScalaCollider)
  .settings(commonSettings)
  .settings(
    name := s"$baseName-Snippets"
  )
*/

////////////////////////// unidoc publishing

// In order to publish only the scala-docs coming out
// of sbt-unidoc, we must create an auxiliary module
// 'pub' depending on the aggregating module 'aggr'.
// There, we copy the setting of `packageDoc`. This way,
// we can publish using `sbt scalacollider-unidoc/publishLocal` etc.
// cf. https://github.com/sbt/sbt-unidoc/issues/65

lazy val aggr: Project = project.in(file("aggr"))
  .enablePlugins(ScalaUnidocPlugin)
  .settings(unidocSettings)
  .aggregate(lList: _*)

lazy val pub: Project = project.withId(s"$baseNameL-unidoc").in(file("pub"))
  .settings(publishSettings)
  .settings(
    name                  := s"$baseName-unidoc",
    version               := PROJECT_VERSION,
    organization          := "de.sciss",
    autoScalaLibrary      := false,
    licenses              := Seq("CC BY-SA 4.0" -> url("https://creativecommons.org/licenses/by-sa/4.0/")), // required for Maven Central
    homepage              := Some(url(s"https://git.iem.at/sciss/$baseName")),
    Compile / packageDoc  := (packageDoc in Compile in aggr).value,
    Compile / packageBin / publishArtifact := false, // there are no binaries
    Compile / packageSrc / publishArtifact := false, // there are no sources
  )
 
lazy val publishSettings = Seq(
  publishMavenStyle := true,
  publishTo :=
    Some(if (isSnapshot.value)
      "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
    else
      "Sonatype Releases"  at "https://oss.sonatype.org/service/local/staging/deploy/maven2"
    ),
  Test / publishArtifact := false,
  pomIncludeRepository := { _ => false },
// N.B.: Bloody site plugin or ghpages already adds scm, then sonatype complains if
// we define it twice
//     <scm>
//      <url>git@git.iem.at:sciss/{n}.git</url>
//      <connection>scm:git:git@git.iem.at:sciss/{n}.git</connection>
//    </scm>
  pomExtra := { val n = baseName
     <developers>
        <developer>
          <id>sciss</id>
          <name>Hanns Holger Rutz</name>
          <url>http://www.sciss.de</url>
        </developer>
      </developers>
  }
)

