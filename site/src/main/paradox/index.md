# ScalaCollider

ScalaCollider is a real-time sound synthesis and signal processing system, based
on [SuperCollider](http://supercollider.sf.net) and the [Scala programming language](http://scala-lang.org/). It can
be used as a _library_ in a Scala application, but there is also a small stand-alone _prototyping environment_ called
ScalaCollider-Swing. See [Mellite](https://www.sciss.de/mellite/) for a more comprehensive IDE.

![ScalaCollider-Swing Screenshot](.../screenshot.jpg)

SuperCollider is one of the most elaborate open source sound synthesis frameworks. It comes with its own language,
sclang, that controls the sound synthesis processes on a server, scsynth. ScalaCollider is an alternative to sclang,
giving you the (perhaps) familiar Scala language to express these sound synthesis processes, and letting you hook up
any other Scala, Java or JVM-based libraries.

ScalaCollider
:   @@snip [Comparison.scala](../../../snippets/src/main/scala/Comparison.scala) { #comparison }

sclang
:   @@snip [Comparison.scd](../../../snippets/src/main/supercollider/Comparison.scd) { #comparison }

ScalaCollider's function is more reduced than sclang's, focusing on UGen graphs and server-side resources such as
buses and buffers. Other functionality is part of the standard Scala library, e.g. collections and GUI. Other
functionality, such as plotting, MIDI, client-side sequencing (Pdefs, Routines, etc.) must be added through
dedicated libraries. The documentation on this site assumes some familiarity with SuperCollider, and will do its
best to help users coming from SuperCollider to understand the basic concepts of Scala and ScalaCollider.

## download

To start hacking straight away, download
the [latest version](https://codeberg.org/sciss/ScalaColliderSwing/releases/latest) of ScalaCollider-Swing.
A version is also maintained on [archive.org](https://archive.org/details/ScalaColliderSwing).

__Note:__ Unfortunately, I don't always manage to upload the latest binaries, so you may find that the
newest version is available for building from source code: [codeberg.org/ScalaCollider](http://codeberg.org/sciss/ScalaCollider)
and [codeberg.org/ScalaColliderSwing](http://codeberg.org/sciss/ScalaColliderSwing).

In order to run ScalaCollider, you also need to have installed on your computer:

- [Java](https://adoptopenjdk.net/) (Java 11 or 8 recommended, Java 9 might cause problems) 
- [SuperCollider](https://supercollider.github.io/download) (version 3.10.x is recommended, but 3.8.x and up should work, too)

## resources

The best way to ask questions, no matter if newbie or expert, is to use the following channel:

- the chat channel at [gitter.im/Sciss/ScalaCollider](https://gitter.im/Sciss/ScalaCollider).
  You need a GitLab, GitHub, or Twitter account to sign in.

The file [ExampleCmd.sc](https://codeberg.org/sciss/ScalaCollider/src/branch/main/ExampleCmd.sc) is a good
starting point for understanding how UGen graphs are written in ScalaCollider. You can directly copy and paste
these examples into the ScalaCollider-Swing prototyping environment. If you start it for the first time, you may
have to adjust the location of the `scsynth` or `scsynth.exe` program in the preferences. Press the boot button to
fire up the SuperCollider server. Then, select an example and press <kbd>Shift</kbd>+<kbd>Return</kbd> to execute.
Hover over a UGen name (e.g. `Mix` or `SinOsc`) and press <kbd>Ctrl</kbd>+<kbd>D</kbd> to open its help file.

For reference, consult the [API documentation](https://www.sciss.de/scalaCollider/latest/api/de/sciss/synth/index.html).
