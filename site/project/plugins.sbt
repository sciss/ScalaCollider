addSbtPlugin("com.typesafe.sbt"      % "sbt-site"    % "1.4.1")
addSbtPlugin("com.eed3si9n"          % "sbt-unidoc"  % "0.4.3")
addSbtPlugin("com.lightbend.paradox" % "sbt-paradox"     % "0.6.9") // "0.3.5" // compatible with sbt-site

